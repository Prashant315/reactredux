import { createStore } from "redux";
import allReducers from "../reducers";
//reducer provide the state for the application 
const store = createStore(allReducers);

export default store;