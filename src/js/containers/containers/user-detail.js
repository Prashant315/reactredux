import React, { Component } from 'react'
import { connect } from 'react-redux'


class UserDetail extends Component {

    render() {
        if (this.props.user == null) {
            return (
                <h2>Select the user...</h2>
            )
        } else {
            return (
                <div>
                    <h2>{this.props.user.id}</h2>
                    <h1>{this.props.user.name}</h1>
                    <p>{this.props.user.address}</p>
                </div>
            )
        }
    }

}
function mapStateToProps(state) {
    return {
        user: state.ActiveUser
    }
}
export default connect(mapStateToProps)(UserDetail)
