import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
// import store from "../js/store/index";
import App from "../js/component/App";
import { createStore } from "redux";
import allReducers from "./reducers";
//reducer provide the state for the application 

const store = createStore(allReducers);

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);