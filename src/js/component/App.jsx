import React from "react";
// import List from "./List";
import UserList from '../containers/containers/user-list'
import UserDetail from "../containers/containers/user-detail";
const App = () => (
    <div className="row mt-5">
        <div className="col-md-4 offset-md-1">
            <h2>UserName List</h2>
            <UserList />
            <hr />
            <h2>User Detail</h2>
            <UserDetail />
        </div>
    </div>
);

export default App;